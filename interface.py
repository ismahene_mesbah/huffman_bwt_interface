# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 21:32:03 2020

@author: Ismahene
"""


import tkinter.messagebox  #bibliotheque qui permet d'afficher des fenetres de messages
from tkinter import *   #biblio tkinter pour les interfaces graphiues
from tkinter.filedialog import askopenfilename   #permet d'ouvrir fenêtre de selection de fichier
from BWT import *  #importation du module qui contient les fonction bwt
from huffman import *  #importation du module huffman
from tkinter import ttk   #impoortation de Treeview pour visualiser les résultats

class App:
    def __init__(self):

       
        self.window = Tk()          
        self.window.title("Graphic interface")
      #  self.window.geometry("1000x500")
        self.window.minsize(600, 460)
   
        self.window.config(background='steelblue4')

        # initialization des composants
        self.frame = Frame(self.window, bg='steelblue4')
        self.inner_frame=Frame(self.frame,bg='steelblue4') #sous frame dans le frame principal

        #creation des noms des colonnes 
        self.colnames=('column 1','column 2')
        
        #création d'une barre déroulante
        self.s=Scrollbar(self.inner_frame)
        
        #creation du treeview qui permet de visualiser seq
        self.tab = ttk.Treeview(self.inner_frame,columns=self.colnames,yscrollcommand=self.s.set)
        self.tab.heading("column 1", text="column 1")
         
        self.tab.column("column 1",minwidth=0,width=100)
        
        self.tab.heading("column 2", text="column 2")
         
        self.tab.column("column 2",minwidth=0,width=100)

        self.s.config(command=self.tab.yview)
        self.window.configure(cursor="hand2")
        # creation des composants
        self.create_widgets()


        # empaquetage
        self.frame.pack(expand=YES)
        self.inner_frame.grid()

    
    def create_widgets(self):
        self.create_title()
        self.create_button()
        self.bwt_b()
        self.bwt_reverse()
        self.permut_button()
        self.sort_button()
        self.huffman_b()
        self.reverse_huffman_b()
        self.game_button()
        self.next_button()

  

      
    
    def create_title(self):
        label_title = Label(self.frame, text=" Burrows Weeler transform & Huffman compression",
                            font=("Courrier", 40), bg='steelblue4',  fg='white')
        label_title.grid(column=0,row=0)



    def create_button(self):
        yt_button = Button(self.frame, text="Upload sequence", font=("Courrier", 20), bg='white', fg='steelblue4',
                           height = 1, width = 40,command=self.fileDialog)
        yt_button.grid(row=1,column=0)

    def fileDialog(self):
        self.chemin=askopenfilename(initialdir =  "/", title = "Select A File",
                                              filetypes =(("Text File", "*.txt"),("fasta", "*.fa"),("fasta","*.fasta*")) ) 
        
    def read_file(self): #permet de lire fichier
        fichier=open(self.chemin,'r')
        f=fichier.readlines()
        seq=""
        for i in f :  
            seq=seq+i[:-1] #éliminer retour chariot
            seq=seq+'$'  #ajouter le caractère $
        return(seq)
    
    def get_word(self):  #donne le mot bwt
        w=""
        s=self.read_file()
        word=get_last_col(s)   #fonction présente dans fichier bwt.py
        w=''.join(word)
        self.tab.delete(*self.tab.get_children())
        self.tab.insert('',1,values=(w))
        self.tab.grid(row=0, column=0, sticky=N+S+E+W)
        return word
    
    
    def bwt_b(self):
        bwt_b = Button(self.frame, text="BWT Word", font=("Courrier", 20), bg='white', fg='steelblue4',
                               height = 1, width = 15, command=self.get_word)
        bwt_b.grid(row=3,column=0,sticky=W)   
    
    def permut_label(self):  #construction matrice
        s=self.read_file()
        matrix=permut(s)  #fonction de construction de la matrice: presente dans bwt.py
        self.tab.delete(*self.tab.get_children())
        for i in matrix:
            self.tab.insert('',1,values=(str(i))) 
        self.tab.grid(row=0, column=0, sticky=N+S+E+W)
        return matrix

        
    def permut_button(self):#bouton matrice
        bwt_b = Button(self.frame, text="Step 1: build matrix", font=("Courrier", 10), bg='white', fg='steelblue4',
                               height = 1, width = 15, command=self.permut_label)

        bwt_b.grid(row=4, column=0,sticky=W)
        
        
    
    def sort_label(self):
        s=self.read_file()
        matrix=sort(s)
        self.tab.delete(*self.tab.get_children())
        for i in matrix:
            self.tab.insert('',len(matrix),values=(i))
        self.tab.grid(row=0, column=1, sticky=N+S+E+W)

        
        
        
    def sort_button(self):
        bwt_b = Button(self.frame, text="Step 2: sorted matrix", font=("Courrier", 10), bg='white', fg='steelblue4',
                               height = 1, width = 15, command=self.sort_label)
 
        bwt_b.grid(row=5, column=0,sticky=W)
    

    def reverse(self): #reconstruction de la matrice a partir du mot bwt
        s=self.read_file()
        matrix=reverse_bwt(s)  #fonction de reconstructon présente dans bwt.py
        self.tab.delete(*self.tab.get_children())
        for i in range(len(matrix)):
            self.tab.insert('',1,values=(matrix[i]))
            
        self.tab.grid(row=0, column=0, sticky=N+S+E+W)
        self.game_button.grid()
        return matrix
    
    def get_value(self): #pour récupérer ce que l'utilisateur veut insérer dans la base de données
        l=[]  
        li=""
        curItem = self.tab.item(self.tab.focus())  #on récupère les lignes séléctionnées par l'utilisateur
        l=curItem['values'] #on met dans une liste les valeurs récupérées
        li=''.join(l)
        
        if li[-1]=="$":
            self.window.configure(cursor="heart")
            tkinter.messagebox.showinfo("Result ", "Bravo !!")
            
        else:
            tkinter.messagebox.showinfo("Result ", "Bad answer =(")
        
            
    def game_button(self): #bouton qui permet d 'insérer les données dans la base de données
        self.game_button = Button(self.frame, text="Submit answer" ,font=("Courrier", 10), bg='white', fg='steelblue4',
                               height = 1, width = 15, command=self.get_value)
        


    
    def bwt_reverse(self):
        bwt_reverse = Button(self.frame, text= "BWT Reverse", font=("Courrier", 20), bg='white', fg='steelblue4',
                                command=self.reverse,height = 1, width = 15)
        bwt_reverse.grid(row=3,column=0,sticky=E+S) 
     
    def show_next(self):  #affiche 2eme etape de huffman ( c a dire dico {binaire : symbole})
        self.tab.delete(*self.tab.get_children())
        for i in self.d:
            self.tab.insert('',1,values=(i))
        self.tab.grid(row=0, column=1, sticky=N+S+E+W)
        
    def next_button(self):  #affiche le dictionnaire {sequence_binaire : symbol}
        self.next=Button(self.frame,text="Compression dico", font=("Courrier", 10), bg='white', fg='steelblue4',
                               height = 1, width = 15,command=self.show_next)
    
    
    def huffman_compression(self): #fonction de compression
        self.obj=Huffman(self.chemin)  #fonction de compression présente dans huffman.py
        self.path,code,self.d,arbre=self.obj.compress()
        self.tab.delete(*self.tab.get_children())
        self.next.grid()
        for i in arbre:
            self.tab.insert('',1,values=(i))
        self.lab=Label(self.frame,text=(code))
        self.buttonRecover = Button(self.frame,
                          text = 'Binary sequence',font=("Courrier", 10), bg='white', fg='steelblue4',
                               height = 1, width = 15,
                          command=lambda: self.lab.grid()).grid(sticky=N)
        self.tab.grid(row=0, column=1, sticky=N+S+E+W)
        tkinter.messagebox.showinfo("Compressed file avaible here ", self.path)
        return(self.path)
        


        
    def huffman_decompression(self):#fonction de décompression
        path,d,txt=self.obj.decompress(self.path)  #fonction de décompression présente dans huffman.py
        tkinter.messagebox.showinfo("Decompressed file available here  ", path)
        self.tab.delete(*self.tab.get_children())
        for j in d:
            self.tab.insert('',1,values=(j))
        lab=Label(self.frame,text=(txt))
        buttonRecover = Button(self.frame,text = 'Original sequence',font=("Courrier", 10), bg='white', fg='steelblue4',
                               height = 1, width = 15,
                          command=lambda: lab.grid()).grid(sticky=S)
        self.tab.grid(row=0, column=1, sticky=N+S+E+W)
        return(path)
    
        
    def show_original(self):#fonction d'affichage de la sequence originale c a d seq décompressée
        path,txt=self.huffman_decompression()
        Label(self.frame,text=("original text  ", txt)).grid()
    
    
    def original_text(self): #bouton qui permet affichage seq décompressée
        self.ob=Button(self.frame,text="Show original text",font=("Courrier", 10), bg='white', fg='steelblue4',
                               height = 1, width = 15, command=show_original)    
    

    def huffman_b(self):
        huffman_b = Button(self.frame, text=" Decompression  ",bg='white', fg='steelblue4',font=("Courrier", 20),command=self.huffman_decompression,height = 1, width = 15)
        huffman_b.grid(row=1,column=0,sticky=E)   


    def reverse_huffman_b(self): #bouton copression
        reverse_huffman_b = Button(self.frame, text="Compression", bg='white', fg='steelblue4',font=("Courrier", 20),command=self.huffman_compression,height = 1, width = 15)

        reverse_huffman_b.grid(row=1,column=0,sticky=W)            

app = App()

app.window.mainloop()