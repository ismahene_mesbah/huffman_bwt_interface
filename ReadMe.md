**Graphical interface Compression/decompression and encrypting/decrypting**


This graphical interface allows to compress / decompress and encrypt / decrypt a DNA sequence
After downloading the three files: huffman.py, BWT.py and interface_bwt_huffman.py, it is the last one that must be executed
When executing the file interface_bwt_huffman.py the graphical interface appears, you must download your file containing the seq
with this program you can:

		* **compress** your file (when you click on the compress button,  the first step appears 
		click on "dico compression " to view the 2nd step (i.e. the binaries dictionary with the corresponding codes).
		a window showing you the path where your compressed file was stored appears
		the "Binary sequence" button allows you to see the sequence in binary

		* **decompress**: as soon as you click on the button a window indicating
		the location where your file was saved appears, you also have each symbol and its
		equivalent in binary, the original sequence button allows you to switch from to DNA and thus view the sequence

		* **encrypt**: apply the BWT algorithm, The " BWT word " button  allows you to directly obtain the word
		the button "step 1" and "step 2" shows you respectively the construction of the matrix before and after lexicographic sorting

		* **decrypt**: after obtaining the word bwt, you reconstruct your original sequence, you only need to press
		the button "reconstruct bwt" ...

		SMALL ORIGINALITY of this interface: once the reconstruction matrix displayed, a very simple game is proposed to you:
		from the matrix you must "guess" which sequence is the original one (it is the one that ends with the symbol "$" ,
		I'm sure you already know that  =)  )
		if you answer correctly the cursor turns into a heart shape and a small window congratulating you will appear!




 ![Demonstration](bwt_huffman.gif)
		