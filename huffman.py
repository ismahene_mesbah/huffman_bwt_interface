# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 20:14:18 2020

@author: Ismahene
"""

import heapq   #biblio heap queue,une implémentation de l'algorithme de file d'attente de tas(tas= arbres binaires)
import os #Ce module fournit une manière portable d'utiliser les fonctionnalités dépendantes du système d'exploitation
from functools import total_ordering #total ordering permet de creer des types ordonnables facilement



@total_ordering
class Arbre:  #classe pour maintenir la structure arboressante
    def __init__(self, char, freq):
        self.char = char #objet caractère
        self.freq = freq   #frequence du caractere
        self.gauche = None  #noeud droit
        self.droite = None #noeud gauche

# définir le comparateur "inférieur "
    def __lt__(self, autre):  #methode lt du module total ordering (librairie functools)==> pour comparer si inf
        return self.freq < autre.freq
    
#définir comparateur " égal"
    def __eq__(self, autre): #methode eq du module total ordering (librairie functools)==> pour comparer si egal
        if(autre == None):
            return False
        if(not isinstance(autre, Arbre)): # isinstance () vérifie si l'objet (premier argument) 
            #est une instance ou une sous-classe de la classe classinfo (deuxième argument).
            return False
        return self.freq == autre.freq


class Huffman:
    def __init__(self, path): #constructeur
        self.path = path #chemin vers le fichier à compresser
        self.arbre = []    #arbre 
        self.codes = {}  #dictionnaire codage
        self.reverse = {}  #dictionnaire décodage

        
        #fonctions de compression
    def dico_freq(self, text):  #retourne dictionnaire avc frequence de chaque lettre
        frequency = {}    #initialiser dico de freq
        for character in text:   #parcourir texte du fichier
            if not character in frequency:
                frequency[character] = 0   
            frequency[character] += 1  #incrémenter de 1 si le caractère existe
        return frequency   

    def cree_arbre(self, frequency):   #creer l'arbre à partir du dico de freq
        for key in frequency:
            noeud = Arbre(key, frequency[key])  #initialiser la classe Arbre avc le dico des freq
            heapq.heappush(self.arbre, noeud)#Poussez l' élément  sur le tas  , c-a-d pousser l'arbre des freq dans self.arbre
       
    def fusion_noeuds(self): #fusionne les noeuds
        while(len(self.arbre)>1):
            noeud1 = heapq.heappop(self.arbre) #renvoie le plus petit element du tas
            noeud2 = heapq.heappop(self.arbre)
            fusion = Arbre(None, noeud1.freq + noeud2.freq) #fusion des deux noeuds plus petits
            fusion.gauche = noeud1  #stocker 1er noeud à gauche
            fusion.droite = noeud2  #stocker 2eme noeud a droite
            heapq.heappush(self.arbre,fusion) #integrer la fusion dans l'arbre self.arbre
        


    def manuel_codage_decodage(self, s, code):
        if(s == None):
            return

        if(s.char != None):
            self.codes[s.char] = code   #remplir dictionnaire de codage 
            self.reverse[code] = s.char  #remplir dico de décodage
            return        #ne rien retourner
        #recursivité
        self.manuel_codage_decodage(s.gauche, code + "0")  #appliquer sur noeud droit et concatener 0 ( 0 pour chemin empunter a droite)
        self.manuel_codage_decodage(s.droite, code + "1") #mettre 1 si chemin gauche emprunté


    def cree_codes(self): #creer les deux dico code et decode  + établir les chemins
        s = heapq.heappop(self.arbre) 
        code = ""
        self.manuel_codage_decodage(s, code)


    def coder_text(self, text): #renvoie le texte codé sous forme de chaine de caractère
        text_code = ""         #initialiser chaine de caractere
        for charactere in text:   #parcourir text
            text_code += self.codes[charactere]  #repmlir chaine de caractere avc les codes du dico "codage"
        return text_code


    def compacter_text(self, text_code):    #compresser texte codé 
        compact = 8 - len(text_code) % 8   #découper motif de taille 8
        for i in range(compact):   
            text_code += "0"   #remplir avc les codes

        info = "{0:08b}".format(compact)
        text_code = info + text_code
       # print(text_codé)
        return text_code


    def tab_byte(self, text_code_compact):  #retourne bytearray
        if(len(text_code_compact) % 8 != 0):
            print("Le text codé n'a pas été compacté correctement")
            exit(0)

        b = bytearray()
        for i in range(0, len(text_code_compact), 8):
            byte = text_code_compact[i:i+8]
            b.append(int(byte, 2))
        return b
    
    def encode(self,frequency):  #cree arbre de huffman
        heap = [[weight, [symbol, '']] for symbol, weight in frequency.items()]
        heapq.heapify(heap)
        while len(heap) > 1:
            lo = heapq.heappop(heap)
            hi = heapq.heappop(heap)
            for pair in lo[1:]:
                pair[1] = '0' + pair[1]
            for pair in hi[1:]:
                pair[1] = '1' + pair[1]
            heapq.heappush(heap, [lo[0] + hi[0]] + lo[1:] + hi[1:])
        return(sorted(heapq.heappop(heap)[1:], key=lambda p: (len(p[-1]), p)))

    def compress(self):  #fonction de copression
        filename, file_extension = os.path.splitext(self.path)
        output_path = filename + ".bin"  #stocker chemin du fichier compressé
        symb=[]
        binaire=[]
        d=[]
        with open(self.path, 'r+') as file, open(output_path, 'wb') as output: #on ouvre fichier input et on ecris dans output
            text = file.read() #lretourner contenu sous forme string
            text = text.rstrip()
            frequency = self.dico_freq(text)  #dico de frequences
            arbre=self.encode(frequency)      #arbre de huffman
            self.cree_arbre(frequency) 
            self.fusion_noeuds()
            self.cree_codes()
            text_code = self.coder_text(text)
            #print(text_code)
            #print(frequency)
            text_code_compact = self.compacter_text(text_code)
            for i in range(0,len(text_code_compact),8):
                binaire.append(text_code_compact[i:i+8])
            #print(binaire)
            b = self.tab_byte(text_code_compact)
            info=[bytes(b[i:i+1]) for i in range (0, len(b))]


            for i in range(len(info)):
              
                d.append([binaire[i],info[i]])
            #print(d)
            output.write(bytes(b))  

       # print("Compressé avc succès")
        return output_path,text_code_compact,d,arbre

    def decompacter(self, text_code_compact):  #décompacte le texte codé compacté
        info = text_code_compact[:8]
        compact = int(info, 2)
        text_code_compact = text_code_compact[8:] 
        text_code = text_code_compact[:-1*compact]
        return text_code

    def decode_text(self, text_code):#part de symboles et retourne texte
        code = ""
        decode_text = ""

        for bit in text_code:
            code += bit
            if(code in self.reverse):
                character = self.reverse[code]
                decode_text += character
                code = ""
        return decode_text


    def decompress(self, input_path): #fonction de décompression
        filename, file_extension = os.path.splitext(self.path)
        output_path = filename + "_decompressed" + ".txt"
        liste_bits=[]
        symb=[]
        d=[]
        with open(input_path, 'rb') as file, open(output_path, 'w') as output:
            bit_string = ""
            byte = file.read(1)
            

            while(len(byte) > 0):
                byte = ord(byte)
                bits = bin(byte)[2:].rjust(8, '0')  #passer de caractere en binaire
                liste_bits.append(bits)
                bit_string += bits
                byte = file.read(1)
                info=[byte[i:i+1] for i in range (0, len(byte))]
                symb.append(info)
                
           # print(symb)    
            text_code = self.decompacter(bit_string)
          #  print(liste_bits)
           

            decompressed_text = self.decode_text(text_code)
          #  print(self.dico_freq(decompressed_text))
            for i in range(len(liste_bits)):
                #print(liste_bits[i])
                d.append([symb[i],liste_bits[i]])
            #print(d)

            
            output.write(decompressed_text)
            output.close()

        #print("Decompressed")
        return output_path, d,decompressed_text #contenu en smtvol , binaire, text 
    
    

    
    
    