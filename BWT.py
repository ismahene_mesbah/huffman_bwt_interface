#!/usr/bin/env python
# coding: utf-8


    
def permut(s):  #fonction qui crée matrice 
    L=len(s)
    matrix=[]
    for i in range(L):
        matrix.append(s[i:] + s[:i])
    return(matrix)
      
    
def sort(s):  #fonction qui sort la matrice en ordre lexicographique
    matrix=sorted(s[i:] + s[:i] for i in range(len(s)))
    return matrix
        
    
        
def get_last_col(s):#fonction qui recupere derniere col
    matrix=sort(s)
    last_column = [row[-1:] for row in matrix] 
    return last_column
    
    
    
def reverse_bwt(s):  #recontruction de la matrice bwt
    bwt=get_last_col(s)
    table = [""] * len(bwt) #creer un tableau vide
    newList=[]
    for i in range(len(bwt)):
        table = sorted(bwt[i] + table[i] for i in range(len(bwt)))
    
    return table